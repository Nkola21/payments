from rest_framework import generics, filters
from django_filters.rest_framework import DjangoFilterBackend

from .serializers import PaymentSerializer
from .models import Payment


class PaymentListView(generics.ListAPIView):
	filter_backends = (DjangoFilterBackend, filters.SearchFilter)
	filterset_fields = ('device_id', )
	search_fields = ('days_until_suspension', 'device_id')
	serializer_class = PaymentSerializer

	def get(self, request, *args, **kwargs):
		return self.list(request, *args, **kwargs)

	def get_queryset(self):
		return Payment.objects.get_successful_payments()


class PaymentDetailView(generics.RetrieveAPIView):
	serializer_class = PaymentSerializer
	lookup_field = 'device_id'

	def get(self, request, *args, **kwargs):
		return self.retrieve(request, *args, **kwargs)

	def get_queryset(self):
		return Payment.objects.get_successful_payments()
