from django.contrib import admin

from .models import Payment


# Register your models here.
@admin.register(Payment)
class PaymentAdmin(admin.ModelAdmin):
	list_display = ('payment_id', 'payment_type', 'payment_amount', 'payment_signiture_img', 'payment_photo', 'created_at', 'status', 'notes', 'agent_user_id', 'device_id')