from drf_enum_field.serializers import EnumFieldSerializerMixin
from rest_framework import serializers

from .models import Payment

class PaymentSerializer(EnumFieldSerializerMixin, serializers.ModelSerializer):

	class Meta:
		model = Payment
		fields = ('payment_id', 'days_until_suspension', 'suspension_date', 'payment_type', 'payment_amount', 'payment_signiture_img', 'payment_photo', 'created_at', 'status', 'notes', 'agent_user_id', 'device_id')
