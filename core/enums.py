from enumfields import Enum  


class PaymentType(Enum):
    CASH = 'CASH'
    DEBIT_ORDER = 'DEBIT_ORDER'
    CLIENT_REFERRAL = 'CLIENT_REFERRAL'
    BANK_DEPOSIT = 'BANK_DEPOSIT'
    CARD = 'CARD'


class PaymentStatus(Enum):
	SUCCESSFUL = 'SUCCESSFUL'
	CANCELLED = 'CANCELLED'
