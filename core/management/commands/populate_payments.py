from django.conf import settings
from django.core.management.base import BaseCommand
from django.utils import timezone
from core.models import Payment
import datetime, decimal
import csv
import pytz


context = decimal.getcontext().copy()
context.prec = 4


class Command(BaseCommand):
    help = "Populate Payment model with data from csv file."

    def handle(self, *args, **options):

        with open('payments.csv', 'rt') as payments:
        	data = csv.reader(payments)

        	for key, row in enumerate(data):
        		if key > 0:
        			populate_model(row)


def populate_model(row):
	payment = Payment()
	payment.payment_id = int(row[0])
	payment.payment_type = row[1]
	payment.payment_amount = decimal.Decimal(row[2])
	payment.payment_signiture_img = row[3]
	payment.payment_photo = row[4]
	payment.created_at = timezone.make_aware(datetime.datetime.strptime(row[5], '%Y-%m-%d %H:%M:%S'), pytz.timezone(settings.TIME_ZONE))
	payment.status = row[6]
	payment.notes = row[7]
	payment.agent_user_id = int(row[8])
	payment.device_id = int(row[9])
	payment.save()