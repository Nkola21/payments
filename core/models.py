from django.conf import settings
from django.db import models

from enumfields import EnumField
from .enums import PaymentType, PaymentStatus


class Payment(models.Model):
	payment_id = models.SmallIntegerField()
	payment_type = EnumField(PaymentType, max_length=15)
	payment_amount = models.DecimalField(max_digits=7, decimal_places=2)
	payment_signiture_img = models.CharField(max_length=200, blank=True, null=True)
	payment_photo = models.CharField(max_length=200, blank=True, null=True)
	created_at = models.DateTimeField()
	status = EnumField(PaymentStatus, max_length=15)
	notes = models.TextField(blank=True, null=True)
	agent_user_id = models.SmallIntegerField()
	device_id = models.SmallIntegerField()

	@property
	def days_until_suspension(self):
		suspension_days = (self.created_at + settings.GRACE_PERIOD) - settings.TODAY_DATE
		return suspension_days.days

	@property
	def suspension_date(self):
		return self.created_at + settings.GRACE_PERIOD

	class PaymentQuerySet(models.QuerySet):

		def get_successful_payments(self):
			return Payment.objects.filter(status=PaymentStatus.SUCCESSFUL.name).order_by('-created_at')

		def get_successful_payment_by_device_id(self, device_id):
			return Payment.objects.filter(device_id=device_id, status=PaymentStatus.SUCCESSFUL.name).order_by('-created_at')

	objects = PaymentQuerySet.as_manager()
