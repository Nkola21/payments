from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from . import views

urlpatterns = [
    path('payments/<int:device_id>/', views.PaymentDetailView.as_view()),
    path('payments/', views.PaymentListView.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)