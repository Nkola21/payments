## Requirements

1. Have python 3 installed


## Setup

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Clone repository.
2. Create virtual environment.
3. activate virtual environment.
4. Run: pip install -r requirements
5. Run command: python manage.py populate_payments
6. Run migrations: python manage.py migrate.
7. Start local server: pyhon manage.py runserver
8. Open(in browser): http://127.0.0.1:8000/api/v1/payments/ for list of payments with an option to filter on device_id. To access specific payment use http://127.0.0.1:8000/api/v1/payments/<device_id>/
---


## Assumptions

1. Amount paid is the correct amount for the that user's plan.
2. Today's date is 15 January 2019
